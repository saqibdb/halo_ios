//
//  baseMapAnnotations.h
//  LookUp
//
//  Created by Divey Punj on 24/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>

@interface baseMapAnnotations : NSObject <MKAnnotation>{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;

-(id)initWithTitle:(NSString *)newTitle subtitle:(NSString*)sub Location:(CLLocationCoordinate2D)location;
-(MKAnnotationView *)annotationView;

@end

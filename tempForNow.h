//
//  tempForNow.h
//  LookUp
//
//  Created by Divey Punj on 19/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <KinveyKit/KinveyKit.h>
#import <Foundation/Foundation.h>

@interface tempForNow : NSObject <KCSPersistable>


@property (nonatomic, copy) NSString *entityId;
@property (nonatomic, copy) CLLocation *geocoord;

-(NSDictionary*)hostToKinveyPropertyMapping;

@end

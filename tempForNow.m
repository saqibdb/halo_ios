//
//  tempForNow.m
//  LookUp
//
//  Created by Divey Punj on 19/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "tempForNow.h"

@implementation tempForNow

-(NSDictionary*)hostToKinveyPropertyMapping{
    
    return @{
             @"entityId": KCSEntityKeyId,
             @"name": @"name",
             @"email": @"email",
             @"gender": @"gender",
             @"status": @"status",
             @"geocoord": KCSEntityKeyGeolocation
             };
}

@end

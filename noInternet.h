//
//  noInternet.h
//  LookUp
//
//  Created by Divey Punj on 25/03/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface noInternet : UIViewController

- (IBAction)checkAgain:(id)sender;

@end

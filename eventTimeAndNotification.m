//
//  eventTimeAndNotification.m
//  LookUp
//
//  Created by Divey Punj on 22/07/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "eventTimeAndNotification.h"

userClass *userClassForEventTime;

@implementation eventTimeAndNotification

-(void)deleteOldEvents{
    
    //Read my events from device
    userClassForEventTime= [[userClass alloc]init];
    NSMutableArray *eventsArray= [[NSMutableArray alloc]initWithArray:[userClassForEventTime getMyEvents]];
    
    //Initialise the return array
    NSMutableArray *cleanedArray= [[NSMutableArray alloc]init];
   //Loop through each array and get the names
    for(int i=0; i< eventsArray.count; i++){
        NSDictionary *tempDict= [[NSDictionary alloc]initWithDictionary:[eventsArray objectAtIndex:i]];
        //Remove the space and time from the back
        NSRange rangeOfStr= [[tempDict objectForKey:@"eventDate"] rangeOfString:@" "];
        NSString* tempStr= [tempDict objectForKey:@"eventDate"];
        if(rangeOfStr.location == NSNotFound){
            
        }
        else{
            tempStr= [tempStr substringToIndex: rangeOfStr.location];
            tempStr= [tempStr stringByAppendingString:@" 00:00:00"];
            
            if([self checkIfDateHasPast:tempStr]){
                //Date is old, so don't add this event to the clean array
            }
            else{
                //Means this event is not old, so keep this in the clean array
                [cleanedArray addObject:[eventsArray objectAtIndex:i]];
            }
        }
    }
    
    //Overwrite my events array onto the device
    [userClassForEventTime setMyEvents:cleanedArray];
}

-(void)setAlarm:(NSString*)dateStr eventId:(NSString*)eventId eventName: (NSString*)eventName{
    
    //Remove the space and time from the back
    NSRange rangeOfStr= [dateStr rangeOfString:@" "];
    if(rangeOfStr.location == NSNotFound){
        
    }
    else{
        dateStr= [dateStr substringToIndex: rangeOfStr.location];
        dateStr= [dateStr stringByAppendingString:@" 16:10:00"];
        
    }
    
    //Set 1 week alarm
    [self convertToDate:dateStr setFor:@"1 week" eventId:eventId eventName:eventName];
    //Set 1 day alarm
    //[self convertToDate:dateStr setFor:@"1 day" eventId:eventId eventName:eventName];
    //Set day alarm
    //[self convertToDate:dateStr setFor:@"day" eventId:eventId eventName:eventName];
}

-(void)convertToDate:(NSString*)dateStr setFor:(NSString*)whenSet eventId:(NSString*)eventId eventName: (NSString*)eventName{
    
    NSDate *dateFromString= [[NSDate alloc]init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyy HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    dateFromString = [dateFormatter dateFromString:dateStr];
    
    if([whenSet isEqualToString:@"1 week"]){
        NSDateComponents *dateComp= [[NSDateComponents alloc] init];
        [dateComp setDay:-7];
        NSDate *oneWkDate= [[NSCalendar currentCalendar] dateByAddingComponents:dateComp toDate:dateFromString options:0];
        [self raiseNotification:oneWkDate stringDate:dateStr eventId:eventId eventName:eventName];
    }
    else if ([whenSet isEqualToString:@"1 day"]){
        NSDateComponents *dateComp= [[NSDateComponents alloc] init];
        [dateComp setDay:-1];
        NSDate *oneDayDate= [[NSCalendar currentCalendar] dateByAddingComponents:dateComp toDate:dateFromString options:0];
        [self raiseNotification:oneDayDate stringDate:dateStr eventId:eventId eventName:eventName];
    }
    else if([whenSet isEqualToString:@"day"]){
        [self raiseNotification:dateFromString stringDate:dateStr eventId:eventId eventName:eventName];
    }
    else{
        //Nothing
    }
    
}

-(void)raiseNotification:(NSDate*) date stringDate: (NSString*) dateString eventId: (NSString*)eventId eventName:(NSString*)eventName{
    
    //Trim dateStr
    NSRange range = [dateString rangeOfString:@" "];
    dateString= [dateString substringToIndex:range.location];
    //Get message string ready
    NSString *message= @"You are going to the ";
    message= [message stringByAppendingString:eventName];
    message= [message stringByAppendingString:@" on "];
    message= [message stringByAppendingString:dateString];
    
    //Create a dictionary to send to user profile
    NSMutableDictionary *tempDict= [[NSMutableDictionary alloc]init];
    [tempDict setValue:message forKey:@"message"];
    [tempDict setValue:date forKey:@"date"];
    [tempDict setValue:eventId forKey:@"eventId"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"raiseLocalEventNotification" object:self userInfo:@{@"eventNotiInfo": tempDict}];
    
}

-(BOOL)checkIfDateHasPast:(NSString*)dateStr{
    
    NSDate *dateFromString= [[NSDate alloc]init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyy HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    dateFromString = [dateFormatter dateFromString:dateStr];
    
    NSDate *todayDate= [NSDate date];
    NSDate *checkDate= dateFromString;
    NSComparisonResult result;
    result = [todayDate compare:checkDate];
    
    if(result == NSOrderedDescending){
        //CheckDate is less
        return YES;
    }
    else{
        return NO;
    }
    
}

@end

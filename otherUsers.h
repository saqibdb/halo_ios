//
//  otherUsers.h
//  LookUp
//
//  Created by Divey Punj on 21/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface otherUsers : NSObject


-(void) initialiseOtherUsers;
-(void)findOtherUsersUsingBluetooth;
-(void)filterOtherUsersWithGender: (NSString *)gender;
-(void)filterOtherUSersWithStatus: (NSString*)status;
-(void)generateOtherUserArray: (NSMutableArray*)usersArray;
-(UIImage*)getOtherUserImage: (NSString*)eID;
-(NSString*)getOtherUserName: (NSString*)eID;



@end






























#ifndef otherUsers_h
#define otherUsers_h


#endif /* otherUsers_h */

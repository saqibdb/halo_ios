//
//  eventsMapViewClass.h
//  LookUp
//
//  Created by Divey Punj on 8/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "eventDialog.h"
#import "userClass.h"
#import "eventMapViewAnnotationClass.h"
#import "events.h"
#import "eventsServerClass.h"
#import "config.h"

@interface eventsMapViewClass : UIViewController{
    MKMapView *eventsMapView;
}

@property (strong, nonatomic) IBOutlet MKMapView *eventsMapView;
@property (strong, nonatomic) eventDialog *activeEventDialog;
@property (strong, nonatomic) IBOutlet UIButton *standardMapBut;
@property (strong, nonatomic) IBOutlet UIButton *satelliteMapBut;
@property (strong, nonatomic) IBOutlet UIButton *hybridMapBut;




-(IBAction)setMap:(id)sender;
- (IBAction)standardMapAction:(id)sender;
- (IBAction)satelliteMapAction:(id)sender;
- (IBAction)hybridMapAction:(id)sender;



@end

//
//  eventCustomCell.m
//  LookUp
//
//  Created by Divey Punj on 8/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "eventCustomCell.h"

@implementation eventCustomCell
@synthesize eventNameLab, eventDateLab, eventNoButton, eventYesButton, eventAddressLab, eventMaybeButton, eventAttendeesLab, eventStatusLab, eventCellBorder;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setCell : (NSString*)eventName eventDate:(NSString*)eventDate eventAddress:(NSString*)eventAddress eventAttendees:(NSString*)eventAttendees isMyEvent: (BOOL)isMyEvent{
    
    eventNameLab.text= eventName;
    eventAddressLab.text= eventAddress;
    eventDateLab.text= eventDate;
    
    eventYesButton.hidden= NO;
    eventNoButton.hidden= NO;
    eventMaybeButton.hidden= NO;
    
    //Convert attendees string into int
    int attendees= [eventAttendees intValue];
    
    if(attendees < 0){
        //Do nothing
    }
    else{
        //eventAttendeesLab.text= [[NSString stringWithFormat:@"%d",attendees] stringByAppendingString:@" people are going to this event."];
        
        eventAttendeesLab.text= [NSString stringWithFormat:@"%d",attendees];
    }
    
    //Check if this event is my event and then remove buttons and add label
    if(isMyEvent){
        eventYesButton.hidden= YES;
        eventNoButton.hidden= YES;
        eventMaybeButton.hidden= YES;
        eventStatusLab.text= @"You are going!";
    }
    else{
        //Nothing
        eventStatusLab.text= @"";
    }
    
    //Set border for cell
    eventCellBorder.layer.borderWidth= 1.0;
    eventCellBorder.layer.borderColor= [[UIColor grayColor]CGColor];
    
    
}

- (IBAction)eventYesAction:(id)sender {
}

- (IBAction)eventNoAction:(id)sender {
}

- (IBAction)eventMaybeAction:(id)sender {
}
@end

//
//  events.m
//  LookUp
//
//  Created by Divey Punj on 7/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "events.h"


ServerQueryBridge *serverBridgeForEvents;
eventsServerClass *eventsServerClassObj;
NSMutableArray *eventsArray;
NSMutableArray *tempEventsArray;
userClass *userClassForEvents;
eventTimeAndNotification *eventTimeForEvent;

@interface events ()

@end

@implementation events
@synthesize eventsListTable, mapViewButton, myEventsButton, eventsView, eventsLoadingLab, eventsViewHolder, eventsLoadIndicator, allEventsBut;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initialise];
    //Get events from server
    [self fetchEvents];
    //ONLY USE THIS TO CLEAR EVENTS SAVED ON DEVICE[self clearAllEventsOnDevice];
}

/*-(void)viewDidAppear:(BOOL)animated{
    
    //Change navigation bar colour
    [self changeNavigationBarColor];
    [super viewDidAppear:YES];
    //[self initialise];
    [self fetchEvents];
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialise{
    
    //DESIGN INITIALISATION
    [self showIndicatorDuringLoad];
    //Create all requred notifications
    //Create a notification for incoming events array
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setEventsTable:) name:@"setEventsTable" object:nil];
    
    //Initialise Classes and variables
    serverBridgeForEvents= [[ServerQueryBridge alloc]init];
    eventsArray= [[NSMutableArray alloc]init];
    eventsServerClassObj= [[eventsServerClass alloc]init];
    userClassForEvents= [[userClass alloc]init];
    tempEventsArray= [[NSMutableArray alloc]init];
    
    //Reload events table
    eventsListTable.delegate= self;
    eventsListTable.dataSource= self;
    
    //Set navigation text colour
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    //Clean my events and delete old events
    eventTimeForEvent= [[eventTimeAndNotification alloc]init];
    [eventTimeForEvent deleteOldEvents];
    
}

-(void)fetchEvents{
    NSLog(@"Fetching Events");
    //[serverBridgeForEvents fetchNearByEvents:EVENTS_WITHIN_RADIUS];
}

-(void)decodeEventsArray:(NSMutableArray*)array{
    eventsArray= array;
    //Notify root controller
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setEventsTable" object:self userInfo:@{@"eventArrayFromServer":array}];
    //Notify Finder as well that events array is ready
    [[NSNotificationCenter defaultCenter] postNotificationName:@"eventsForFinderView" object:self userInfo:@{@"eventsForProfile": array}];

}

-(void)setEventsTable:(NSNotification*)notification{
    
    //Remove notification
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"setEventsTable" object:nil];
    //Show table view
    [self removeIndicatorAfterLoad];
    //Call AllEvents action
    [self allEventsAction:0];

}

//Segue to message dialog
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"openEventDialog"]){
      
        self.activeEventDialog= segue.destinationViewController;
        NSInteger eventIndex= [[self.eventsListTable indexPathForCell:(UITableViewCell *)sender]row];
        //Get selected event data and push forward
        eventsServerClassObj= [tempEventsArray objectAtIndex:eventIndex];
        self.activeEventDialog.eventName= eventsServerClassObj.eventName;
        self.activeEventDialog.eventDate= eventsServerClassObj.eventDateTime;
        self.activeEventDialog.eventAddress= eventsServerClassObj.eventAddress;
        self.activeEventDialog.eventAttendees= eventsServerClassObj.eventAttendees;
        self.activeEventDialog.eventId= eventsServerClassObj.eventId;
       
        return;
    }
    
}

-(NSMutableArray*)getEventsArray{
    return eventsArray;
}

- (IBAction)myEventsAction:(id)sender {

    //Change colour of button
    [myEventsButton setBackgroundColor:[UIColor blueColor]];
    //Set all other buttons to gray
    [allEventsBut setBackgroundColor:[UIColor grayColor]];
    [mapViewButton setBackgroundColor:[UIColor grayColor]];
    
    //Read my events from device
    NSMutableArray* myEvents= [[NSMutableArray alloc]initWithArray:[userClassForEvents getMyEvents]];
    NSMutableArray* tempEvents= [[NSMutableArray alloc]init];
    //Check if these events are in tempEventsArray, if so, then delete the unmatched ones
    for(int i=0; i<tempEventsArray.count; i++){
        for(int j=0; j<myEvents.count; j++){
            eventsServerClassObj= [tempEventsArray objectAtIndex:i];
            if([[[myEvents objectAtIndex:j] objectForKey:@"eventId"] isEqualToString: eventsServerClassObj.eventId]){
                [tempEvents addObject:eventsServerClassObj];
            }
        }
    }
    //Clear tempEventsArray
    [tempEventsArray removeAllObjects];
    //Add tempEvents
    tempEventsArray= tempEvents;
    [eventsListTable reloadData];
    
}

- (IBAction)allEventsAction:(id)sender {
    //Change colour of button
    [allEventsBut setBackgroundColor:[UIColor blueColor]];
    //Set all other buttons to gray
    [myEventsButton setBackgroundColor:[UIColor grayColor]];
    [mapViewButton setBackgroundColor:[UIColor grayColor]];

    tempEventsArray= [[NSMutableArray alloc]initWithArray:eventsArray];
    //NSLog(@"Events Array: %@",eventsArray);
    //Reload the table
    [eventsListTable reloadData];
}

-(void)showIndicatorDuringLoad{
    //Hide table view
    eventsView.hidden= YES;
    //Start the indicator
    eventsLoadIndicator.hidden= NO;
    [eventsLoadIndicator startAnimating];
    eventsLoadingLab.text= @"Loading events..";
}

-(void)removeIndicatorAfterLoad{
    //Fade in the message contacts
    [eventsView setAlpha:0.0];
    eventsView.hidden= NO;
    [eventsViewHolder addSubview:eventsView];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [eventsView setAlpha:1];
    [UIView commitAnimations];
    //Stop the indicator and hide message
    [eventsLoadIndicator stopAnimating];
    eventsLoadIndicator.hidden= YES;
    eventsLoadingLab.text= @"";
}
-(void)noEventsAfterLoad{
    [eventsLoadIndicator stopAnimating];
    eventsLoadIndicator.hidden= YES;
    eventsLoadingLab.text= @"You don't have any mates in your contacts..";
}


//******************************TABLE VIEW METHODS**********************************
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tempEventsArray.count==0){
        return 0;
    }
    else{
        return tempEventsArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    eventCustomCell *cell= [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
    
    if(cell == nil){
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: @"eventCell"];
    }
    
    if(tempEventsArray.count==0){
        //Do nothing
    }
    else{
        eventsServerClassObj= [tempEventsArray objectAtIndex:indexPath.row];
    }
    
    mapViewButton.hidden= NO;
    
    NSString *eventName= eventsServerClassObj.eventName;
    NSString *eventDate= eventsServerClassObj.eventDateTime;
    NSString *eventAddress= eventsServerClassObj.eventAddress;
    NSString *eventAttendees= eventsServerClassObj.eventAttendees;

    [cell setCell:eventName eventDate:eventDate eventAddress:eventAddress eventAttendees:eventAttendees isMyEvent:NO];
    
    return cell;
}


-(void)clearAllEventsOnDevice{
    
    userClassForEvents= [[userClass alloc]init];
    
    NSMutableArray* tempArray  = [[NSMutableArray alloc]init];
    [userClassForEvents setMyEvents:tempArray];
    
    NSLog(@"All events on device deleted");
}










@end

//
//  signUp.h
//  LookUp
//
//  Created by Divey Punj on 14/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "AppDelegate.h"
#import "config.h"
#import <SAMKeychain/SAMKeychain.h>
#import "Reachability.h"
#import "userSettings.h"

@interface signUp : UIViewController <FBSDKLoginButtonDelegate, UITextFieldDelegate>{

    UIImage *userImage;
}

//PROPERTIES
@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmationText;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIScrollView *signUpScrollView;
@property (strong, nonatomic) IBOutlet UIView *signupView;

@property (strong, nonatomic) IBOutlet UIView *loadIndView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;
@property (strong, nonatomic) IBOutlet UILabel *loadingText;


//ACTIONS
- (IBAction)signupSubmit:(id)sender;
- (IBAction)userAgreementCheckAction:(id)sender;
- (IBAction)cancelAction:(id)sender;



@end

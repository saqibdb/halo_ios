//
//  eventCustomCell.h
//  LookUp
//
//  Created by Divey Punj on 8/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eventCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *eventNameLab;
@property (strong, nonatomic) IBOutlet UILabel *eventAddressLab;
@property (strong, nonatomic) IBOutlet UILabel *eventDateLab;
@property (strong, nonatomic) IBOutlet UILabel *eventAttendeesLab;
@property (strong, nonatomic) IBOutlet UIButton *eventYesButton;
@property (strong, nonatomic) IBOutlet UIButton *eventNoButton;
@property (strong, nonatomic) IBOutlet UIButton *eventMaybeButton;
@property (strong, nonatomic) IBOutlet UILabel *eventStatusLab;
@property (strong, nonatomic) IBOutlet UILabel *eventCellBorder;



- (IBAction)eventYesAction:(id)sender;
- (IBAction)eventNoAction:(id)sender;
- (IBAction)eventMaybeAction:(id)sender;
- (void) setCell : (NSString*)eventName eventDate:(NSString*)eventDate eventAddress:(NSString*)eventAddress eventAttendees:(NSString*)eventAttendees isMyEvent: (BOOL)isMyEvent;


@end

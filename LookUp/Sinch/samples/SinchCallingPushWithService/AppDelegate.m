#import "AppDelegate.h"

@interface AppDelegate () <SINServiceDelegate, SINCallClientDelegate>
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  id config = [[SinchService configWithApplicationKey:@"<APPLICATION KEY>"
                                    applicationSecret:@"<APPLICATION SECRET>"
                                      environmentHost:@"sandbox.sinch.com"]
      pushNotificationsWithEnvironment:SINAPSEnvironmentAutomatic];

  id<SINService> sinch = [SinchService serviceWithConfig:config];
  sinch.delegate = self;
  sinch.callClient.delegate = self;

  void (^onUserDidLogin)(NSString *) = ^(NSString *userId) {
    [sinch logInUserWithId:userId];
    [sinch.push registerUserNotificationSettings];
  };

  self.sinch = sinch;

  [[NSNotificationCenter defaultCenter] addObserverForName:@"UserDidLoginNotification"
                                                    object:nil
                                                     queue:nil
                                                usingBlock:^(NSNotification *note) {
                                                  onUserDidLogin(note.userInfo[@"userId"]);
                                                }];

  return YES;
}

- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  [self.sinch.push application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  [self.sinch.push application:application didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
  NSLog(@"%@:%@", NSStringFromSelector(_cmd), error);
}

#pragma mark - SINCallClientDelegate

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
  // Find MainViewController and present CallViewController from it.
  UIViewController *top = self.window.rootViewController;
  while (top.presentedViewController) {
    top = top.presentedViewController;
  }
  [top performSegueWithIdentifier:@"callView" sender:call];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
  SINLocalNotification *notification = [[SINLocalNotification alloc] init];
  notification.alertAction = @"Answer";
  notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
  return notification;
}

#pragma mark - SINServiceDelegate

- (void)service:(id<SINService>)service didFailWithError:(NSError *)error {
  NSLog(@"%@", [error localizedDescription]);
}

- (void)service:(id<SINService>)service
     logMessage:(NSString *)message
           area:(NSString *)area
       severity:(SINLogSeverity)severity
      timestamp:(NSDate *)timestamp {
  if (severity == SINLogSeverityCritical) {
    NSLog(@"%@", message);
  }
}

@end
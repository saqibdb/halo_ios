//
//  FileStore.swift
//  Kinvey
//
//  Created by Victor Barros on 2016-02-04.
//  Copyright © 2016 Kinvey. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper


#if !os(macOS)
    import UIKit
#endif

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

public enum ImageRepresentation {
    
    case png
    case jpeg(compressionQuality: Float)

#if !os(macOS)
    func data(image: UIImage) -> Data? {
        switch self {
        case .png:
            return UIImagePNGRepresentation(image)
        case .jpeg(let compressionQuality):
            return UIImageJPEGRepresentation(image, CGFloat(compressionQuality))
        }
    }
#endif
    
    var mimeType: String {
        switch self {
        case .png: return "image/png"
        case .jpeg: return "image/jpeg"
        }
    }
    
}

/// Class to interact with the `Files` collection in the backend.
open class FileStore {
    
    public typealias FileCompletionHandler = (File?, Swift.Error?) -> Void
    public typealias FileDataCompletionHandler = (File?, Data?, Swift.Error?) -> Void
    public typealias FilePathCompletionHandler = (File?, URL?, Swift.Error?) -> Void
    public typealias UIntCompletionHandler = (UInt?, Swift.Error?) -> Void
    public typealias FileArrayCompletionHandler = ([File]?, Swift.Error?) -> Void
    
    internal let client: Client
    internal let cache: FileCache?
    
    /// Factory method that returns a `FileStore`.
    open class func getInstance(_ client: Client = sharedClient) -> FileStore {
        return FileStore(client: client)
    }
    
    fileprivate init(client: Client) {
        self.client = client
        self.cache = client.cacheManager.fileCache(fileURL: client.fileURL())
    }

#if !os(macOS)
    /// Uploads a `UIImage` in a PNG or JPEG format.
    @discardableResult
    open func upload(_ file: File, image: UIImage, imageRepresentation: ImageRepresentation = .png, ttl: TTL? = nil, completionHandler: FileCompletionHandler? = nil) -> Request {
        let data = imageRepresentation.data(image: image)!
        file.mimeType = imageRepresentation.mimeType
        return upload(file, data: data, ttl: ttl, completionHandler: completionHandler)
    }
#endif
    
    /// Uploads a file using the file path.
    @discardableResult
    open func upload(_ file: File, path: String, ttl: TTL? = nil, completionHandler: FileCompletionHandler? = nil) -> Request {
        return upload(file, fromData: nil, fromFile: URL(fileURLWithPath: path), ttl: ttl, completionHandler: completionHandler)
    }
    
    /// Uploads a file using a input stream.
    @discardableResult
    open func upload(_ file: File, stream: InputStream, ttl: TTL? = nil, completionHandler: FileCompletionHandler? = nil) -> Request {
        let data = NSMutableData()
        stream.open()
        var buffer = [UInt8](repeating: 0, count: 4096)
        while stream.hasBytesAvailable {
            let read = stream.read(&buffer, maxLength: buffer.count)
            data.append(buffer, length: read)
        }
        stream.close()
        return upload(file, data: data as Data, ttl: ttl, completionHandler: completionHandler)
    }

    fileprivate func getFileMetadata(_ file: File, ttl: TTL? = nil) -> (request: Request, promise: Promise<File>) {
        let request = self.client.networkRequestFactory.buildBlobDownloadFile(file, ttl: ttl)
        let promise = Promise<File> { fulfill, reject in
            request.execute({ (data, response, error) -> Void in
                if let response = response , response.isOK,
                    let json = self.client.responseParser.parse(data),
                    let newFile = File(JSON: json) {
                    newFile.path = file.path
                    if let cache = self.cache {
                        cache.save(newFile, beforeSave: nil)
                    }
                    
                    fulfill(newFile)
                } else {
                    reject(buildError(data, response, error, self.client))
                }
            })
        }
        return (request: request, promise: promise)
    }
    
    /// Uploads a file using a `NSData`.
    @discardableResult
    open func upload(_ file: File, data: Data, ttl: TTL? = nil, completionHandler: FileCompletionHandler? = nil) -> Request {
        return upload(file, fromData: data, fromFile: nil, ttl: ttl, completionHandler: completionHandler)
    }
    
    /// Uploads a file using a `NSData`.
    fileprivate func upload(_ file: File, fromData: Data?, fromFile: URL?, ttl: TTL? = nil, completionHandler: FileCompletionHandler? = nil) -> Request {
        if file.size.value == nil {
            if let data = fromData {
                file.size.value = Int64(data.count)
            } else if let url = fromFile,
                let attrs = try? FileManager.default.attributesOfItem(atPath: url.path),
                let fileSize = attrs[.size] as? Int64
            {
                file.size.value = fileSize
            }
        }
        let requests = MultiRequest()
        Promise<(file: File, skip: Int?)> { fulfill, reject in //creating bucket
            let createUpdateFileEntry = {
                let request = self.client.networkRequestFactory.buildBlobUploadFile(file)
                requests += request
                request.execute { (data, response, error) -> Void in
                    if let response = response , response.isOK,
                        let json = self.client.responseParser.parse(data),
                        let newFile = File(JSON: json) {
                        
                        fulfill((file: newFile, skip: nil))
                    } else {
                        reject(buildError(data, response, error, self.client))
                    }
                }
            }
            
            if let _ = file.fileId,
                let uploadURL = file.uploadURL
            {
                var request = URLRequest(url: uploadURL)
                request.httpMethod = "PUT"
                if let uploadHeaders = file.uploadHeaders {
                    for header in uploadHeaders {
                        request.setValue(header.1, forHTTPHeaderField: header.0)
                    }
                }
                request.setValue("0", forHTTPHeaderField: "Content-Length")
                if let data = fromData {
                    request.setValue("bytes */\(data.count)", forHTTPHeaderField: "Content-Range")
                } else if let fromFile = fromFile,
                    let attrs = try? FileManager.default.attributesOfItem(atPath: (fromFile.path as NSString).expandingTildeInPath),
                    let fileSize = attrs[FileAttributeKey.size] as? UInt
                {
                    request.setValue("bytes */\(fileSize)", forHTTPHeaderField: "Content-Range")
                }
                
                if self.client.logNetworkEnabled {
                    do {
                        log.debug("\(request)")
                    }
                }
                
                let dataTask = self.client.urlSession.dataTask(with: request) { (data, response, error) in
                    if self.client.logNetworkEnabled, let response = response as? HTTPURLResponse {
                        do {
                            log.debug("\(response.description(data))")
                        }
                    }
                    
                    let regexRange = try! NSRegularExpression(pattern: "[bytes=]?(\\d+)-(\\d+)", options: [])
                    if let response = response as? HTTPURLResponse , 200 <= response.statusCode && response.statusCode < 300 {
                        createUpdateFileEntry()
                    } else if let response = response as? HTTPURLResponse,
                        response.statusCode == 308,
                        let rangeString = response.allHeaderFields["Range"] as? String,
                        let textCheckingResult = regexRange.matches(in: rangeString, options: [], range: NSMakeRange(0, rangeString.characters.count)).first,
                        textCheckingResult.numberOfRanges == 3
                    {
                        let rangeNSString = rangeString as NSString
                        let endRangeString = rangeNSString.substring(with: textCheckingResult.rangeAt(2))
                        if let endRange = Int(endRangeString) {
                            fulfill((file: file, skip: endRange))
                        } else {
                            reject(Error.invalidResponse(httpResponse: response, data: data))
                        }
                    } else {
                        reject(buildError(data, HttpResponse(response: response), error, self.client))
                    }
                }
                requests += URLSessionTaskRequest(client: client, task: dataTask)
                dataTask.resume()
            } else {
                createUpdateFileEntry()
            }
        }.then { file, skip in //uploading data
            return Promise<File> { fulfill, reject in
                var request = URLRequest(url: file.uploadURL!)
                request.httpMethod = "PUT"
                if let uploadHeaders = file.uploadHeaders {
                    for header in uploadHeaders {
                        request.setValue(header.1, forHTTPHeaderField: header.0)
                    }
                }
                
                let handle: (Data?, URLResponse?, Swift.Error?) -> Void = { data, response, error in
                    if self.client.logNetworkEnabled, let response = response as? HTTPURLResponse {
                        do {
                            log.debug("\(response.description(data))")
                        }
                    }
                    
                    if let response = response as? HTTPURLResponse, 200 <= response.statusCode && response.statusCode < 300 {
                        if let fileURL = fromFile {
                            file.path = fileURL.path
                        }
                        
                        fulfill(file)
                    } else {
                        reject(buildError(data, HttpResponse(response: response), error, self.client))
                    }
                }
                
                if let data = fromData {
                    let uploadData: Data
                    if let skip = skip {
                        let startIndex = skip + 1
                        uploadData = data.subdata(in: startIndex ..< data.count - startIndex)
                        request.setValue("bytes \(startIndex)-\(data.count - 1)/\(data.count)", forHTTPHeaderField: "Content-Range")
                    } else {
                        uploadData = data
                    }
                    
                    if self.client.logNetworkEnabled {
                        do {
                            log.debug("\(request.description)")
                        }
                    }
                    
                    let uploadTask = self.client.urlSession.uploadTask(with: request, from: uploadData) { (data, response, error) -> Void in
                        handle(data, response, error)
                    }
                    requests += (URLSessionTaskRequest(client: self.client, task: uploadTask), addProgress: true)
                    uploadTask.resume()
                } else if let fromFile = fromFile {
                    if self.client.logNetworkEnabled {
                        do {
                            log.debug("\(request.description)")
                        }
                    }
                    
                    let uploadTask = self.client.urlSession.uploadTask(with: request, fromFile: fromFile) { (data, response, error) -> Void in
                        handle(data, response, error)
                    }
                    requests += (URLSessionTaskRequest(client: self.client, task: uploadTask), addProgress: true)
                    uploadTask.resume()
                } else {
                    reject(Error.invalidResponse(httpResponse: nil, data: nil))
                }
            }
        }.then { file in //fetching download url
            return self.getFileMetadata(file, ttl: ttl).1
        }.then { file in
            completionHandler?(file, nil)
        }.catch { error in
            completionHandler?(file, error)
        }
        return requests
    }
    
    /// Refresh a `File` instance.
    @discardableResult
    open func refresh(_ file: File, ttl: TTL? = nil, completionHandler: FileCompletionHandler? = nil) -> Request {
        let fileMetadata = getFileMetadata(file, ttl: ttl)
        let request = fileMetadata.0
        fileMetadata.1.then { file in
            completionHandler?(file, nil)
        }.catch { error in
            completionHandler?(file, error)
        }
        return request
    }
    
    @discardableResult
    fileprivate func downloadFileURL(_ file: File, storeType: StoreType = .cache, downloadURL: URL) -> (request: URLSessionTaskRequest, promise: Promise<URL>) {
        let downloadTaskRequest = URLSessionTaskRequest(client: client, url: downloadURL)
        let promise = Promise<URL> { fulfill, reject in
            let executor = Executor()
            downloadTaskRequest.downloadTaskWithURL(file) { (url: URL?, response, error) in
                if let response = response , response.isOK || response.isNotModified, let url = url {
                    if storeType == .cache {
                        var pathURL: URL? = nil
                        var entityId: String? = nil
                        executor.executeAndWait {
                            entityId = file.fileId
                            pathURL = file.pathURL
                        }
                        if let pathURL = pathURL , response.isNotModified {
                            fulfill(pathURL)
                        } else {
                            let fileManager = FileManager()
                            if let entityId = entityId,
                                let baseFolder = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                            {
                                do {
                                    var baseFolderURL = URL(fileURLWithPath: baseFolder)
                                    baseFolderURL = baseFolderURL.appendingPathComponent(self.client.appKey!).appendingPathComponent("files")
                                    if !fileManager.fileExists(atPath: baseFolderURL.path) {
                                        try fileManager.createDirectory(at: baseFolderURL, withIntermediateDirectories: true, attributes: nil)
                                    }
                                    let toURL = baseFolderURL.appendingPathComponent(entityId)
                                    if fileManager.fileExists(atPath: toURL.path) {
                                        do {
                                            try fileManager.removeItem(atPath: toURL.path)
                                        }
                                    }
                                    try fileManager.moveItem(at: url, to: toURL)
                                    
                                    if let cache = self.cache {
                                        cache.save(file) {
                                            file.path = NSString(string: toURL.path).abbreviatingWithTildeInPath
                                            file.etag = response.etag
                                        }
                                    }
                                    
                                    fulfill(toURL)
                                } catch let error {
                                    reject(error)
                                }
                            } else {
                                reject(Error.invalidResponse(httpResponse: response.httpResponse, data: nil))
                            }
                        }
                    } else {
                        fulfill(url)
                    }
                } else {
                    reject(buildError(nil, response, error, self.client))
                }
            }
        }
        return (request: downloadTaskRequest, promise: promise)
    }
    
    @discardableResult
    fileprivate func downloadFileData(_ file: File, downloadURL: URL) -> (request: URLSessionTaskRequest, promise: Promise<Data>) {
        let downloadTaskRequest = URLSessionTaskRequest(client: client, url: downloadURL)
        let promise = downloadTaskRequest.downloadTaskWithURL(file).then { data, response -> Promise<Data> in
            return Promise<Data> { fulfill, reject in
                fulfill(data)
            }
        }
        return (request: downloadTaskRequest, promise: promise)
    }
    
    /// Returns the cached file, if exists.
    open func cachedFile(_ entityId: String) -> File? {
        if let cache = cache {
            return cache.get(entityId)
        }
        return nil
    }
    
    /// Returns the cached file, if exists.
    open func cachedFile(_ file: inout File) {
        guard let entityId = file.fileId else {
            fatalError("File.entityId is required")
        }
        
        if let cachedFile = cachedFile(entityId) {
            file = cachedFile
        }
    }
    
    fileprivate func crashIfInvalid(file: File) {
        guard let _ = file.fileId else {
            fatalError("File.entityId is required")
        }
    }
    
    /// Downloads a file using the `downloadURL` of the `File` instance.
    @discardableResult
    open func download(_ file: File, storeType: StoreType = .cache, ttl: TTL? = nil, completionHandler: FilePathCompletionHandler? = nil) -> Request {
        crashIfInvalid(file: file)
        
        if storeType == .sync || storeType == .cache,
            let entityId = file.fileId,
            let cachedFile = cachedFile(entityId),
            file.pathURL != nil
        {
            DispatchQueue.main.async {
                completionHandler?(cachedFile, cachedFile.pathURL, nil)
            }
        }
        
        if storeType == .cache || storeType == .network {
            let multiRequest = MultiRequest()
            Promise<(File, URL)> { fulfill, reject in
                if let downloadURL = file.downloadURL, file.publicAccessible || file.expiresAt?.timeIntervalSinceNow > 0 {
                    fulfill((file, downloadURL))
                } else {
                    let (request, promise) = getFileMetadata(file, ttl: ttl)
                    multiRequest += request
                    promise.then { (file) -> Void in
                        if let downloadURL = file.downloadURL {
                            fulfill((file, downloadURL))
                        } else {
                            throw Error.invalidResponse(httpResponse: nil, data: nil)
                        }
                    }.catch { error in
                        reject(error)
                    }
                }
            }.then { (file, downloadURL) -> Promise<(File, URL)> in
                let (request, promise) = self.downloadFileURL(file, storeType: storeType, downloadURL: downloadURL)
                multiRequest += (request, true)
                return promise.then { localUrl in
                    return Promise<(File, URL)> { fulfill, reject in
                        fulfill((file, localUrl))
                    }
                }
            }.then { file, localUrl -> Void in
                completionHandler?(file, localUrl, nil)
            }.catch { [file] error in
                completionHandler?(file, nil, error)
            }
            return multiRequest
        } else {
            return LocalRequest()
        }
    }
    
    /// Downloads a file using the `downloadURL` of the `File` instance.
    @discardableResult
    open func download(_ file: File, ttl: TTL? = nil, completionHandler: FileDataCompletionHandler? = nil) -> Request {
        crashIfInvalid(file: file)
        
        if let entityId = file.fileId, let cachedFile = cachedFile(entityId), let path = file.path, let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
            DispatchQueue.main.async {
                completionHandler?(cachedFile, data, nil)
            }
        }
        
        let multiRequest = MultiRequest()
        Promise<(File, URL)> { fulfill, reject in
            if let downloadURL = file.downloadURL , file.publicAccessible || file.expiresAt?.timeIntervalSinceNow > 0 {
                fulfill((file, downloadURL))
            } else {
                let (request, promise) = getFileMetadata(file, ttl: ttl)
                multiRequest += request
                promise.then { file -> Void in
                    if let downloadURL = file.downloadURL , file.publicAccessible || file.expiresAt?.timeIntervalSinceNow > 0 {
                        fulfill(file, downloadURL)
                    } else {
                        throw Error.invalidResponse(httpResponse: nil, data: nil)
                    }
                }.catch { error in
                    reject(error)
                }
            }
        }.then { (file, downloadURL) -> Promise<Data> in
            let (request, promise) = self.downloadFileData(file, downloadURL: downloadURL)
            multiRequest += (request, addProgress: true)
            return promise
        }.then { data in
            completionHandler?(file, data, nil)
        }.catch { error in
            completionHandler?(file, nil, error)
        }
        return multiRequest
    }
    
    /// Deletes a file instance in the backend.
    @discardableResult
    open func remove(_ file: File, completionHandler: UIntCompletionHandler? = nil) -> Request {
        let request = client.networkRequestFactory.buildBlobDeleteFile(file)
        Promise<UInt> { fulfill, reject in
            request.execute({ (data, response, error) -> Void in
                if let response = response , response.isOK,
                    let json = self.client.responseParser.parse(data),
                    let count = json["count"] as? UInt
                {
                    if let cache = self.cache {
                        cache.remove(file)
                    }
                    
                    fulfill(count)
                } else {
                    reject(buildError(data, response, error, self.client))
                }
            })
        }.then { count in
            completionHandler?(count, nil)
        }.catch { error in
            completionHandler?(nil, error)
        }
        return request
    }
    
    /// Gets a list of files that matches with the query passed by parameter.
    @discardableResult
    open func find(_ query: Query = Query(), ttl: TTL? = nil, completionHandler: FileArrayCompletionHandler? = nil) -> Request {
        let request = client.networkRequestFactory.buildBlobQueryFile(query, ttl: ttl)
        Promise<[File]> { fulfill, reject in
            request.execute({ (data, response, error) -> Void in
                if let response = response , response.isOK,
                    let jsonArray = self.client.responseParser.parseArray(data)
                {
                    var files: [File] = []
                    for json in jsonArray {
                        //let file = File()
                        //self.fillFile(file, json: json)
                        if let file = Mapper<File>().map(JSON: json) {
                            files.append(file)
                        }
                    }
                    fulfill(files)
                } else {
                    reject(buildError(data, response, error, self.client))
                }
            })
        }.then { files in
            completionHandler?(files, nil)
        }.catch { error in
            completionHandler?(nil, error)
        }
        return request
    }
    
    /**
     Clear cached files from local storage.
     */
    open func clearCache() {
        client.cacheManager.clearAll()
    }
    
}

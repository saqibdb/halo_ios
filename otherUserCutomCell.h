//
//  otherUserCutomCell.h
//  LookUp
//
//  Created by Divey Punj on 20/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface otherUserCutomCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *otherUserNameLab;
@property (weak, nonatomic) IBOutlet UILabel *otherUserStatusLab;
@property (weak, nonatomic) IBOutlet UILabel *otherUserInterestsLab;
@property (weak, nonatomic) IBOutlet UIImageView *otherUserImageView;



- (IBAction)otherUserTalkButton:(id)sender;
- (void) setCell : (NSString *)name status:(NSString *)status interest:(NSString *)interest location:(NSString *)location image:(NSString *)imageName;
-(void)rnd: (NSString*)rd;

@end

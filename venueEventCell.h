//
//  venueEventCell.h
//  LookUp
//
//  Created by Divey Punj on 24/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface venueEventCell : UITableViewCell

@property (strong, nonatomic)IBOutlet UILabel *venueName;
@property (strong, nonatomic)IBOutlet UILabel *eventName;
@property (strong, nonatomic)IBOutlet UILabel *eventDate;
@property (strong, nonatomic)IBOutlet UILabel *eventAddress;


@end

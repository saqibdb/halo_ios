//
//  tutorial.m
//  LookUp
//
//  Created by Divey Punj on 6/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "tutorial.h"
int imgNum;

@interface tutorial ()

@end

@implementation tutorial

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    imgNum=0;
    _doneTutBut.hidden= YES;
    _prevTutBut.hidden= YES;
    [self nextTut:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextTut:(id)sender {
    
    imgNum++;
    
    if(imgNum>5){
        imgNum=5;
    }
    
    if(imgNum==1){
        _tutImg.image= [UIImage imageNamed:@"Tut1.png"];
        _prevTutBut.hidden=YES;
    }
    else if(imgNum==2){
        _tutImg.image= [UIImage imageNamed:@"Tut2.png"];
        _prevTutBut.hidden= NO;
    }
    else if(imgNum==3){
        _tutImg.image= [UIImage imageNamed:@"Tut3.png"];
    }
    else if(imgNum==4){
        _tutImg.image= [UIImage imageNamed:@"Tut4.png"];
    }
    else{
        _tutImg.image= [UIImage imageNamed:@"Tut5.png"];
        _nextTutBut.hidden= YES;
        _doneTutBut.hidden= NO;
    }
}

- (IBAction)prevTut:(id)sender {
    
    imgNum--;
    
    if(imgNum<1){
        imgNum=1;
    }
    
    if(imgNum==4){
        _tutImg.image= [UIImage imageNamed:@"Tut4.png"];
        _doneTutBut.hidden= YES;
        _nextTutBut.hidden= NO;
    }
    else if(imgNum==3){
        _tutImg.image= [UIImage imageNamed:@"Tut3.png"];
    }
    else if(imgNum==2){
        _tutImg.image= [UIImage imageNamed:@"Tut2.png"];
    }
    else if(imgNum==1){
        _tutImg.image= [UIImage imageNamed:@"Tut1.png"];
        _prevTutBut.hidden= YES;
    }
    else{
        //Nothing
    }
    
}

- (IBAction)doneTut:(id)sender {
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"tutorialComplete" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"signUpToAddDel" object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

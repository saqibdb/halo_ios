//
//  passwordReset.h
//  LookUp
//
//  Created by Divey Punj on 5/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ServerQueryBridge.h"
#import <SAMKeychain/SAMKeychain.h>


@interface passwordReset : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLab;
@property (strong, nonatomic) IBOutlet UILabel *errorInfoLab;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIButton *gotologin;

@property (strong, nonatomic) IBOutlet UIImageView *testBarcodeImage;


- (IBAction)submitAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)gotologin:(id)sender;





@end

//
//  initialLoginView.h
//  LookUp
//
//  Created by Divey Punj on 14/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ServerQueryBridge.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegate.h"
#import "Reachability.h"

@interface initialLoginView : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *beforeLab;
@property (strong, nonatomic) IBOutlet UIImageView *img_man;
@property (strong, nonatomic) IBOutlet UIImageView *img_bubble;
@property (strong, nonatomic) IBOutlet UIImageView *img_wom1;
@property (strong, nonatomic) IBOutlet UIImageView *img_wom2;
@property (strong, nonatomic) IBOutlet UIImageView *img_wom3;
@property (strong, nonatomic) IBOutlet UILabel *afterInfo;
@property (strong, nonatomic) IBOutlet UIImageView *showApp;

@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *signupButton;
@property (strong, nonatomic) IBOutlet UIView *show_view;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) IBOutlet UILabel *loadText;
@property (strong, nonatomic) IBOutlet UIButton *replayShowBut;



- (IBAction)facebookLogin:(id)sender;
- (IBAction)loginAction:(id)sender;
- (IBAction)signupAction:(id)sender;
- (IBAction)replayShowAction:(id)sender;




@end

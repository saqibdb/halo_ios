//
//  location.h
//  LookUp
//
//  Created by Divey Punj on 21/02/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "ServerQueryBridge.h"
#import "userSettings.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>

//const int locationCalcTimer= 5;

@interface location : NSObject <CLLocationManagerDelegate>{
    
    //Create locationManager
    CLLocationManager *locationManager;
}


@property (weak, nonatomic) NSString *userLatitude;
@property (weak, nonatomic) NSString *userLongitude;



-(void) initialiseLocation;
-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
-(void) locationManager: (CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
-(NSString*) getLocationLatitude;
-(NSString*) getLocationLongitude;
-(void) getLocation;
-(void) locationCalculator;
-(void)fetchLocation;


@end

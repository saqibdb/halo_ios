//
//  baseMapAnnotations.m
//  LookUp
//
//  Created by Divey Punj on 24/04/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "baseMapAnnotations.h"

@implementation baseMapAnnotations
@synthesize title,coordinate, subtitle;

-(id)initWithTitle:(NSString *)newTitle subtitle:(NSString*)sub Location:(CLLocationCoordinate2D)location{
    self= [super init];
    
    if(self){
        title = newTitle;
        coordinate = location;
        subtitle= sub;
    }
    return self;
}

-(MKAnnotationView*)annotationView{
    MKAnnotationView *annotationView= [[MKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"baseMapAnnotations"];
    
    annotationView.enabled= YES;
    annotationView.canShowCallout= YES;
    annotationView.image= [UIImage imageNamed:@"mapAnnot_halo copy.png"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
}




@end

//
//  beaconClass.m
//  LookUp
//
//  Created by Divey Punj on 15/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "beaconClass.h"
#import "peripheralView.h"

peripheralView *peripheralViewClass;

@implementation beaconClass
@synthesize beaconRegion, beaconPeripheralData;

-(void)initialise{
    
    //Create proximity object
    NSUUID *proximityUUID= [[NSUUID alloc]initWithUUIDString:TRANSFER_SERVICE_UUID];
    
    //Create a region
    beaconRegion= [[CLBeaconRegion alloc]initWithProximityUUID:proximityUUID identifier:@"com.mpdp.LookUp"];
    beaconPeripheralData= [beaconRegion peripheralDataWithMeasuredPower:nil];
    self.peripheralManager= [[CBPeripheralManager alloc]initWithDelegate:self queue:nil options:nil];
    
}

-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    NSLog(@"Peripheral Manager did update state");
    
    /*switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOff:
            NSLog(@"Bluetooth is off!");
            break;
        case CBPeripheralManagerStateResetting:
            NSLog(@"Bluetooth resetting");
            break;
        case CBPeripheralManagerStateUnknown:
            NSLog(@"Bluetooth is on");
            //Start advertising beacon data
            [self.peripheralManager startAdvertising:beaconPeripheralData];
            break;
        case CBPeripheralManagerStateUnsupported:
            NSLog(@"Bluetooth Unsupported");
            break;
        default:
            NSLog(@"Bluetooth state unknown");
            break;
    }*/
    
    if(peripheral.state==CBPeripheralManagerStatePoweredOn){
        //Bluetooth is on
        NSLog(@"Bluetooth is on");
        [self.peripheralManager startAdvertising:beaconPeripheralData];
        [peripheralViewClass initiliase];
    }
    else if(peripheral.state==CBPeripheralManagerStatePoweredOff){
        NSLog(@"Bluetooth is off!");
        
    }
    else if(peripheral.state== CBPeripheralManagerStateUnsupported){
        NSLog(@"Broadcasting unsupported");
    }
    
    
}

















@end

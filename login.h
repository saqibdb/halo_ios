//
//  login.h
//  LookUp
//
//  Created by Divey Punj on 10/04/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerQueryBridge.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Reachability.h"
#import "emailVerificationViewController.h"

@interface login : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic, retain) UIViewController *logInView;
@property (strong, nonatomic) IBOutlet UIScrollView *loginScrollView;
@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) emailVerificationViewController *emailVeriCont;

- (IBAction)loginAction:(id)sender;
-(void)loginFailed;
- (IBAction)loginCancelButtonAction:(id)sender;


@end

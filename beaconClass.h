//
//  beaconClass.h
//  LookUp
//
//  Created by Divey Punj on 15/05/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import "SERVICES.h"


@interface beaconClass : NSObject <CBPeripheralManagerDelegate>

@property (strong, nonatomic) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic) NSDictionary *beaconPeripheralData;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;

-(void)initialise;

@end

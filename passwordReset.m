//
//  passwordReset.m
//  LookUp
//
//  Created by Divey Punj on 5/05/2017.
//  Copyright © 2017 mpdp. All rights reserved.
//

#import "passwordReset.h"
ServerQueryBridge *serverForPassReset;
NSString *entityId_PassReset;

@interface passwordReset ()

@end

@implementation passwordReset

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    serverForPassReset= [[ServerQueryBridge alloc]init];
    [serverForPassReset initialiseBackend];

    _errorInfoLab.text= @"";
    [_submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [_submitButton setEnabled:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(passwordResetResult:) name:@"passwordResetResult" object:nil];
    
    _gotologin.hidden= YES;
    
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyboard:)];
    tap.cancelsTouchesInView= NO;
    [self.view addGestureRecognizer:tap];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitAction:(id)sender {
    _errorInfoLab.text= @"";
    if([_emailAddress.text isEqualToString:@""]||_emailAddress.text== nil){
        _errorInfoLab.text= @"Email address cannot be empty. Please enter your Halo email address";
    }
    else{
        [_submitButton setTitle:@"Working.." forState:UIControlStateNormal];
        [_submitButton setEnabled:NO];
        [serverForPassReset resetPassword:_emailAddress.text];
    }
}

- (IBAction)cancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)gotologin:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)passwordResetResult:(NSNotification*)notification{

    _errorInfoLab.text= @"A password reset email has been sent to your email address. Please follow the instructions and Log in again";
    
    [_submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [_submitButton setEnabled:YES];
    
    _gotologin.hidden= NO;
    
}

-(void) closeKeyboard: (UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}



@end

//
//  bumpMotionClass.m
//  LookUp
//
//  Created by Divey Punj on 27/03/2016.
//  Copyright © 2016 mpdp. All rights reserved.
//

#import "bumpMotionClass.h"
#import "userProfile.h"
#import "userClass.h"
#import "ServerQueryBridge.h"
#import "usersTalkingStore.h"
#import "otherUsers.h"
#import "popupMessageBox.h"

CMMotionManager *motionManager;
userProfile *userProfClass;
userClass *thisUserClass;
ServerQueryBridge *dataServer;
usersTalkingStore *talkingStore;
otherUsers *otherUserData;
popupMessageBox *messageBox;

NSMutableArray *usersTalkingData;
NSTimer *dataFetchTimer;
NSString* dateStr, *timeStr;
double x,y,z;
double myBumpValue;
BOOL clearToBumpAgain;

@implementation bumpMotionClass

-(void)initliaseBumpDetection{
    //Motion Manager Initialisation
    motionManager= [[CMMotionManager alloc]init];
    motionManager.showsDeviceMovementDisplay= YES;
    motionManager.deviceMotionUpdateInterval= 1.0/60.0;
    [motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXMagneticNorthZVertical];
    userProfClass= [[userProfile alloc]init];
    thisUserClass= [[userClass alloc]init];
    [thisUserClass userInitialise];
    dataServer= [[ServerQueryBridge alloc]init];
    [dataServer initialise];
    talkingStore= [[usersTalkingStore alloc]init];
    otherUserData= [[otherUsers alloc]init];
    
    usersTalkingData= [[NSMutableArray alloc]init];
    dateStr= @"";
    timeStr= @"";
    x=0.0;
    y=0.0;
    z=0.0;
    myBumpValue= 0.0;
    messageBox= [[popupMessageBox alloc]init];
    clearToBumpAgain= YES;
}

-(void)startBumpDetection{
    //Previously tried values .4678
    double accl= motionManager.deviceMotion.userAcceleration.x;
   
    if(accl>.5){
        //Pause the timer
        //Notify user profile
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pauseTimer" object:self];
        
        if(clearToBumpAgain == YES){
            clearToBumpAgain= NO;
            [self bumpCheck:accl];
        }
        
    }
    
}

-(void)bumpCheck :(double)accl{
    myBumpValue= accl;
    NSLog(@"Bump Detected: %f", motionManager.deviceMotion.userAcceleration.x);
    
    //Get the date and time, convert it to a string and seperate date and time
    NSDate *dateNow= [NSDate date];
    NSDateFormatter *df= [[NSDateFormatter alloc]init];
    [df setDateStyle:NSDateFormatterMediumStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
    NSCalendar *calendar= [NSCalendar currentCalendar];
    NSDateComponents *components= [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:dateNow];
    //Create date string
    dateStr= [NSString stringWithFormat:@"%ld/%ld/%ld",(long)[components day],(long)[components month],(long)[components year]];
    //Create time string
    timeStr= [NSString stringWithFormat:@"%ld:%ld:%ld",(long)[components hour],(long)[components minute],(long)[components second]];
    
    //Find out if this user is talking
    BOOL isTalking= [userProfClass getIsTalking];
    //If is talking, then save data
    if(isTalking==YES){
        [dataServer setTalkingToUser:[userProfClass getuserTalkingToId] beingTalkedByUser:[thisUserClass getEntityId] otherUserBumpValue:[NSString stringWithFormat:@"%g",accl]otherUserBumpDate:dateStr otherUserBumpTime:timeStr otherUserName:[thisUserClass getName] otherUsersEmail:[thisUserClass getEmail]];
        [userProfClass setIsTalking:NO];
    }
    else{
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotBumpData:) name:@"gotBumpData" object:nil];
        //Being talked to, so check who left data for me on server, filter using my id and date
        [dataServer getTalkingData:[thisUserClass getEntityId] date:dateStr];
    }

}

-(void)gotBumpData:(NSNotification*)notification{
    [self getSingleBumpedUser:[notification object]];
}

-(void)getSingleBumpedUser:(NSMutableArray*)array{
    usersTalkingData= array;
    NSMutableArray *finalSingleUserArray= [[NSMutableArray alloc]init];
    NSString *beingTalkedbyUserId;
    NSString *otherUserName= @"";
    NSString *otherUserEmail= @"";
        if(usersTalkingData.count>0){
            //Read through array and analyse data
            for (int i=0; i<usersTalkingData.count; i++) {
                talkingStore= [usersTalkingData objectAtIndex:i];
                beingTalkedbyUserId= talkingStore.beingTalkedByUser;
                NSString *otherUserBumpValue= talkingStore.otherUserBumpValue;
                NSString *otherUserBumpTime= talkingStore.otherUserBumpTime;
                
                
                //Check for time first
                NSDateComponents *myTimeComp= [self strToDate:dateStr time:timeStr];
                NSDateComponents *otherTimeComp= [self strToDate:dateStr time:otherUserBumpTime];
                if([myTimeComp hour] == [otherTimeComp hour]){
                    if(labs([myTimeComp minute]-[otherTimeComp minute])<3){
                        if(labs(([myTimeComp second]-[otherTimeComp second]))<100){
                            //Match Bump value
                            double otherBumpValue= [otherUserBumpValue doubleValue];
                            if(fabs((myBumpValue-otherBumpValue)*1000)<5000){
                                //Match found
                                [finalSingleUserArray addObject:[usersTalkingData objectAtIndex:i]];
                                otherUserName= talkingStore.otherUserName;
                                otherUserEmail= talkingStore.otherUserEmail;
                            }
                        }
                    }
                }
                
                
            }
            //Check if final array only has one person, if not give error and restart, else say this person wants to add you.
            
            if(finalSingleUserArray.count==1){
                
                //Get other user image
                UIImage* image= [otherUserData getOtherUserImage:beingTalkedbyUserId];
                
                //Ask user for adding contact
                NSString *message= [@"You have been bumped by: " stringByAppendingString:otherUserName];
                message= [message stringByAppendingString:@". Would you like to add this person to your contacts?"];
                
                //Send message and photo to popup message box
                [userProfClass showMessageBox:message otherUsersImage:image otherUserId:beingTalkedbyUserId email:otherUserEmail];
                [finalSingleUserArray removeAllObjects];
                clearToBumpAgain= YES;

                
            }
            else{
                //Send error message to user profile
                [userProfClass bumpMessage:@"Unable to detect bump, please bump again"];
                [finalSingleUserArray removeAllObjects];
                [usersTalkingData removeAllObjects];
                clearToBumpAgain= YES;
                
                
            }
        }
        else{
            //Ask for data again
            [dataServer getTalkingData:[thisUserClass getEntityId] date:dateStr];
            [usersTalkingData removeAllObjects];
            clearToBumpAgain= YES;
            
        }
}

-(NSDateComponents*)strToDate:(NSString*)date time:(NSString*)time{
    
    NSString *tempStr= [date stringByAppendingString:@" "];
    NSString *tempDateTime= [tempStr stringByAppendingString:time];
    
    NSDateFormatter *tempDate= [[NSDateFormatter alloc]init];
    [tempDate setDateFormat:@"dd/MM/yyy HH:mm:ss"];
    [tempDate setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *finalDate= [tempDate dateFromString:tempDateTime];
    
    NSCalendar *calendar= [NSCalendar currentCalendar];
    NSDateComponents *components= [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:finalDate];
    return components;

}
                   

@end
